package com.example.demo.film;

import com.example.demo.genres.Genre;
import com.example.demo.genres.GenresService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.IdGenerator;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FilmsServiceTest {
    public static final String GENRE_ID = "GENREID";
    public static final String FILMNAME = "FILM1";
    public static final int YEAR = 2020;
    private static final UUID ID = UUID.randomUUID();

    @Mock
    FilmsRepository filmsRepository;
    @Mock
    GenresService genresService;
    @Mock
    IdGenerator idGenerator;

    @InjectMocks
    FilmsService filmsService;


    @Test
    void shouldAddFilm() {
        Genre genre = new Genre();
        genre.setId(GENRE_ID);
        Film film = getFilm(genre, null);

        when(genresService.genreExists(genre)).thenReturn(true);
        when(idGenerator.generateId()).thenReturn(ID);

        filmsService.add(film);

        verify(filmsRepository).save(getFilm(genre, ID.toString()));
    }

    @Test
    void shouldAddFilmFailWhenGenreNotExists() {
        Genre genre = new Genre();
        genre.setId(GENRE_ID);
        Film film = getFilm(genre, null);

        when(genresService.genreExists(genre)).thenReturn(false);

        assertThatThrownBy(() -> filmsService.add(film))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Genre not exists");

        verify(idGenerator, never()).generateId();
        verify(filmsRepository, never()).save(any());
    }

    private Film getFilm(Genre genre, String id) {
        Film film = new Film();
        film.setId(id);
        film.setGenre(genre);
        film.setName(FILMNAME);
        film.setYear(YEAR);
        return film;
    }
}