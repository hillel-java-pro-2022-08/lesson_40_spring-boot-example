package com.example.demo.genres;

import com.example.demo.controllers.ErrorController;
import com.example.demo.controllers.FilmsController;
import com.example.demo.film.Film;
import com.example.demo.film.FilmsService;
import com.example.demo.testutil.GenresUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.restdocs.RestDocsMockMvcConfigurationCustomizer;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = {FilmsController.class, ErrorController.class})
@AutoConfigureRestDocs
public class FilmsControllerTest {

    @MockBean
    GenresService genresService;
    @MockBean
    FilmsService filmsService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @TestConfiguration
    static class RestDocsConfiguration {
        @Bean
        public RestDocsMockMvcConfigurationCustomizer restDocsMockMvcConfigurationCustomizer() {
            return configurer -> configurer.operationPreprocessors().withResponseDefaults(Preprocessors.prettyPrint());
        }

    }


    @Test
    void testGetGeneres() throws Exception {
        when(genresService.findAll()).thenReturn(List.of(GenresUtil.GENRE1, GenresUtil.GENRE2));

        ResultActions resp = mockMvc.perform(MockMvcRequestBuilders.get("/genres"))
                .andDo(print());

        resp.andExpect(status().isOk())
                .andExpect(content().json(
                        objectMapper.writeValueAsString(List.of(GenresUtil.GENRE1, GenresUtil.GENRE2))
                ));

        resp.andDo(document(
                "get-generes-success",
                responseFields(
                        fieldWithPath("[].name").type("string").description("genre name"),
                        fieldWithPath("[].id").type("string").description("genre id")
                )
        ));

    }

    @Test
    void testAddFilm() throws Exception {

        ResultActions resp = mockMvc.perform(MockMvcRequestBuilders.post("/films").content("""
                                {
                                    "name":"film1",
                                    "genre":{
                                        "id":"HRR"
                                     },
                                     "year":2020
                                }
                                """
                        ).contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print());

        resp.andExpect(status().isOk())
                .andExpect(content().json(
                        """
                                {"status":"OK"}
                                """
                ));

        Film film = new Film();
        film.setName("film1");
        film.setYear(2020);
        film.setGenre(GenresUtil.createGenre("HRR", null));
        verify(filmsService).add(film);


        resp.andDo(document(
                "add-film-success",
                responseFields(
                        fieldWithPath("status").type("string").description("Operation result OK or FAIL"),
                        fieldWithPath("error").type("string").optional().description("error text")
                ),
                requestFields(
                        fieldWithPath("name").description("film name"),
                        fieldWithPath("year").description("film production year"),
                        fieldWithPath("genre").description("film genre information"),
                        fieldWithPath("genre.id").description("id of film genre")
                )
        ));

        doThrow(new IllegalArgumentException("errortext")).when(filmsService).add(any());


        resp = mockMvc.perform(
                        MockMvcRequestBuilders.post("/films")
                                .content("{}")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print());

        resp.andExpect(status().is4xxClientError())
                .andExpect(content().json(
                        """
                                {"status":"FAIL","error":"errortext"}
                                """
                ));

        resp.andDo(document("add-film-fail"));


    }
}
