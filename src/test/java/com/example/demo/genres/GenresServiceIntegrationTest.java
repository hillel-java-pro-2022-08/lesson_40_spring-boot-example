package com.example.demo.genres;

import com.example.demo.testutil.GenresUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GenresServiceIntegrationTest {



    @Autowired
    GenresService genresService;


    @Autowired
    GenresRepository genresRepository;

    @BeforeEach
    @Transactional
    void setUp(){
        genresRepository.deleteAll();
        genresRepository.save(GenresUtil.GENRE1);
        genresRepository.save(GenresUtil.GENRE2);
    }


    @Test
    @Transactional
    void shouldGetGenres(){
        List<Genre> genres = genresService.findAll();
        assertThat(genres).containsExactlyInAnyOrder(GenresUtil.GENRE1,GenresUtil.GENRE2);
    }
}