package com.example.demo.testutil;

import com.example.demo.genres.Genre;

public class GenresUtil {
    public final static Genre GENRE1 = createGenre("ONE","First");
    public final static Genre GENRE2 = createGenre("TWO","SECOND");

    public static Genre createGenre(String one, String first) {
        Genre genre = new Genre();
        genre.setId(one);
        genre.setName(first);
        return genre;
    }
}
