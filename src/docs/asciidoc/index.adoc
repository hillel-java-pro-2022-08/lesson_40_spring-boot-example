= My Application Docs
== Films Controller Docs

=== Get Genres
http request example:
include::{snippets}/get-generes-success/http-request.adoc[]
response body:
include::{snippets}/get-generes-success/response-body.adoc[]
response body parameters:
include::{snippets}/get-generes-success/response-fields.adoc[]

[source,java]
----
private final static int X=10;
----


[plantuml,auth-protocol]
----
interface X{
    -id:String
    +getName():String
    ~example(i:int)
}

class Y{
    -id:String
    +getName():String
    ~example(i:int)
}

Y..|>X

----


[plantuml,auth-protocol2]
----
participant a
participant service_b
database users


a->users: SELECT * FROM users
users->a
a->service_b: GET /users

----
