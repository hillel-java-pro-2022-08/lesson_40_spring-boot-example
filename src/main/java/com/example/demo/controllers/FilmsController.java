package com.example.demo.controllers;

import com.example.demo.common.PageResponse;
import com.example.demo.film.Film;
import com.example.demo.film.FilmsService;
import com.example.demo.genres.Genre;
import com.example.demo.genres.GenresService;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class FilmsController {

    private final FilmsService filmsService;
    private final GenresService genresService;


    @GetMapping("genres")
    List<Genre> getGenres(@RequestParam(required = false) String name) {
        return name==null ? genresService.findAll() : genresService.findByName(name);
    }

    @GetMapping("films")
    PageResponse<Film> getFilms(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "year", required = false) Integer year,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "genreId", required = false) String genreId
    ) {
        return filmsService.find(name, genreId, year, page);
    }


    @PostMapping("films")
    Map<String, String> addFilm(@RequestBody Film film) {
        filmsService.add(film);
        return Map.of("status", "OK");
    }
}
