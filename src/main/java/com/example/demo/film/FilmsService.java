package com.example.demo.film;

import com.example.demo.common.PageResponse;
import com.example.demo.genres.GenresService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.util.IdGenerator;

import java.util.UUID;

@RequiredArgsConstructor
@Service
public class FilmsService {
    private static final int PAGE_SIZE = 20;

    private final FilmsRepository filmsRepository;
    private final GenresService genresService;
    private final IdGenerator idGenerator;

    public void add(Film film) {
        if (film.getName() == null) throw new IllegalArgumentException("Film name must be not null");
        if (!genresService.genreExists(film.getGenre())) throw new IllegalArgumentException("Genre not exists");

        film.setId(idGenerator.generateId().toString());

        filmsRepository.save(film);
    }

    public PageResponse<Film> find(String name, String genreId, Integer year, Integer page) {
        page = page == null ? 1 : page;
        Page<Film> films = filmsRepository.find(name, genreId, year, page - 1, PAGE_SIZE);
        return new PageResponse<>(page, films.getTotalPages(), films.getContent());
    }
}
