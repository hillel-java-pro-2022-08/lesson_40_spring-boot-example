package com.example.demo.film;

import com.example.demo.genres.Genre;
import jakarta.persistence.*;
import lombok.Data;
@Data
@Entity
@Table(name = "films")
public class Film {
    @Id
    String id;
    String name;
    @ManyToOne
    @JoinColumn(name = "genre_id")
    Genre genre;
    Integer year;
}
