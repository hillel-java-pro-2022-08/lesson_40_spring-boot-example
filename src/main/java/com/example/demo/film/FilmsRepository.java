package com.example.demo.film;

import jakarta.persistence.criteria.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


//ORM object reletion mapping
// JPA - Java Persistence Api
// Hibernate , EclipseLink

public interface FilmsRepository extends JpaRepository<Film, String>, JpaSpecificationExecutor<Film> {

    default Page<Film> find(String name, String genreId, Integer year, int page, int pageSize) {
        return findAll((Root<Film> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
            Join genreJoin = root.join("genre", JoinType.LEFT);
            Predicate p = cb.and();
            if (name != null) {
                p = cb.and(p, cb.like(root.get("name"), "%" + name + "%"));
            }
            if (genreId != null) {
                p = cb.and(p, cb.equal(genreJoin.get("id"), genreId));
            }
            if (year != null) {
                p = cb.and(p, cb.equal(root.get("year"), year));
            }
            return p;
        }, PageRequest.of(page, pageSize));
    }

}
