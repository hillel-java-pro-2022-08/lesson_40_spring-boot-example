package com.example.demo.common;

import lombok.Value;

import java.util.List;

@Value
public class PageResponse<T>{
    int page;
    int pageCount;
    List<T> data;
}
