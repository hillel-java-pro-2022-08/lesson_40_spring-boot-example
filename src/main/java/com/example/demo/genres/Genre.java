package com.example.demo.genres;

import com.example.demo.film.Film;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name = "geners")
public class Genre {
    @Id
    String id;
    String name;
    @JsonIgnore
    @OneToMany(mappedBy = "genre")
    List<Film> filmList;
}
