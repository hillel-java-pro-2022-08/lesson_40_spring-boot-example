package com.example.demo.genres;

import jakarta.persistence.LockModeType;
import jakarta.persistence.QueryHint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GenresRepository extends JpaRepository<Genre, String> {
    List<Genre> findByNameLike(String name);

    @QueryHints(
            @QueryHint(name = "javax.persistence.lock.timeout", value = "-2")
    )
    @Lock(LockModeType.PESSIMISTIC_READ)
    @Query("select g from Genre g where g.name like :name")
    List<Genre> find(@Param("name") String name);


    @Query(value = "SELECT id,name FROM geners WHERE name LIKE :name", nativeQuery = true)
    List<Genre> find2(@Param("name") String name);

}
