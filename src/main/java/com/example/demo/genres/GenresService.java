package com.example.demo.genres;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class GenresService {
    private final GenresRepository genresRepository;

    public List<Genre> findAll() {
        return genresRepository.findAll();
    }

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
    public List<Genre> findByName(String name) {
        List<Genre> g = genresRepository.find(name + "%");

        g.forEach(gn -> System.out.println(gn.getFilmList()));// SELECT FILMS

        return g;
    }

    public boolean genreExists(Genre genre) {
        return genresRepository.findById(genre.getId()).isPresent();
    }




}
